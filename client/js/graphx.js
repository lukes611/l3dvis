
function L3DV()
{
	this.renderer = new THREE.WebGLRenderer({ antialias: true });
	this.w = window.innerWidth;
	this.h = window.innerHeight;
	this.renderer.setSize(this.w,this.h);
	document.body.appendChild(this.renderer.domElement);
	this.camera = new THREE.PerspectiveCamera(45, this.w / this.h, 0.01, 10000);
	this.camera.position.x = 0;
	this.camera.position.y = 0;
	this.camera.position.z = 500;
	this.light = new THREE.PointLight(0xFFFFFF);
	this.light.position.set(0, 300, 200);
	this.list = [];
	this.load_cube();
}

L3DV.prototype.load_cube = function()
{
	var cubeGeometry = new THREE.CubeGeometry(60, 60, 60);
	var cubeMaterial = this.material(0xFFFFFF);
	this.list.push(new THREE.Mesh(cubeGeometry, cubeMaterial));
};

L3DV.prototype.load = function(d)
{
	if(d.type == 'si')
	{
		var mat = this.material(0xFFFFFF);
		var geo = new THREE.Geometry();
		for(var i = 0; i < d.points.length; i++)
			geo.vertices.push(new THREE.Vector3(d.points[i].x,d.points[i].y,d.points[i].z));
		for(var i = 0; i < d.triangles.length; i++)
			geo.faces.push(new THREE.Face3(d.triangles[i].x, d.triangles[i].y, d.triangles[i].z));
		geo.computeBoundingSphere();
		geo.computeBoundingBox();
		geo.computeFaceNormals();
		this.list = [new THREE.Mesh(geo, mat)];
	}
	if(d.type == 'obj')
	{
		var mat = this.material(0xFFFFFF);
		var geo = new THREE.Geometry();
		var has_normals = d.normals.length > 0;
		for(var i = 0; i < d.points.length; i++)
			geo.vertices.push(new THREE.Vector3(d.points[i].x,d.points[i].y,d.points[i].z));
		for(var i = 0; i < d.triangles.length; i++)
		{
			if(!has_normals)
				geo.faces.push(new THREE.Face3(d.triangles[i].x, d.triangles[i].y, d.triangles[i].z));
			else
			{
				var n1 = new THREE.Vector3(d.normals[d.normalis[i].x].x,d.normals[d.normalis[i].x].y,d.normals[d.normalis[i].x].z);
				var n2 = new THREE.Vector3(d.normals[d.normalis[i].y].x,d.normals[d.normalis[i].y].y,d.normals[d.normalis[i].y].z);
				var n3 = new THREE.Vector3(d.normals[d.normalis[i].z].x,d.normals[d.normalis[i].z].y,d.normals[d.normalis[i].z].z);
				
				geo.faces.push(new THREE.Face3(d.triangles[i].x, d.triangles[i].y, d.triangles[i].z, 
				/*vert normals*/[
					n1,
					n2,
					n3
				]));
			}
		}
		geo.computeBoundingSphere();
		geo.computeBoundingBox();
		if(!has_normals)
			geo.computeFaceNormals();
		var mesh = new THREE.Mesh(geo, mat);
		this.list = [mesh];
	}else if(d.type == 'pix3d')
	{
		var geo = new THREE.Geometry();
		var cols = [];
		for(var i = 0; i < d.points.length; i++)
		{
			geo.vertices.push(new THREE.Vector3(d.points[i].x, d.points[i].y, d.points[i].z));
			cols.push(new THREE.Color(d.colors[i].g/255, d.colors[i].g/255, d.colors[i].r/255));
		}
		geo.colors = cols;
		var mat = new THREE.PointsMaterial({
			size : 1,
			vertexColors : THREE.VertexColors
		});
		var pcloud = new THREE.Points(geo, mat);
		this.list = [pcloud];
	}else if(d.type == 'img')
	{
		var me = this;
		var texture = new THREE.TextureLoader();
		var url = '/textureload?full_path=' + d.fname;
		texture.load(url, function(tex){
			
			
			var geometry = new THREE.CubeGeometry(tex.image.width, tex.image.height, tex.image.width);
			var material = new THREE.MeshBasicMaterial({map : tex});
			var mesh = new THREE.Mesh(geometry, material);
			me.list = [mesh];
		});
	}
};

L3DV.prototype.material = function(color)
{
	return new THREE.MeshLambertMaterial({ color: color })
};

L3DV.prototype.draw = function()
{
	this.light.position.set(this.camera.position.x,this.camera.position.y,this.camera.position.z);
	this.camera.lookAt(new THREE.Vector3(0,0,0));
	
	var scene = new THREE.Scene;
	
	scene.add(this.light);
	for(var i = 0; i < this.list.length; i++)
		scene.add(this.list[i]);
	
	this.renderer.render(scene, this.camera);
};

L3DV.prototype.move_camera_z = function(x)
{
	this.camera.position.z -= x;
};

L3DV.prototype.rotate = function(anglex, angley){
	anglex /= 180 / Math.PI;
	angley /= 180 / Math.PI;
	this.list.forEach(function(ob){
		ob.rotation.y += angley;
		ob.rotation.x += anglex;
	});
};