var fs = require('fs');

exports.load = function(fname, cb)
{
	fs.readFile(fname, function(err, d)
	{
		if(err)
		{
			cb({type:"si", fname:fname, error:true});
		}else
		{
			var rv = {type:"si", fname:fname, error:false};
			rv.points = [];
			rv.triangles = [];
			var lines = d.toString().split('\n');
			var num_verts = Number(lines.shift());
			var num_tris = Number(lines.shift());
			for(var i = 0; i < num_verts; i++)
			{
				var n = lines.shift().replace(/\n/g, '').split(' ');
				rv.points.push({x:Number(n[0]), y:Number(n[1]), z:Number(n[2])});
			}
			for(var i = 0; i < num_tris; i++)
			{
				var n = lines.shift().replace(/\n/g, '').split(' ');
				rv.triangles.push({x:Number(n[0]), y:Number(n[1]), z:Number(n[2])});
			}
			cb(rv);
		}
	});
};



