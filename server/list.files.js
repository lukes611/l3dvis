var fs = require('fs');

function list_files(cb)
{
	var rx = /^(\s|\S)+.(jpg|png|gif|bmp|pix3d|obj|si){1}$/;
	var fn = './locations.txt';
	fs.readFile(fn, function(err, d)
	{
		if(err)
		{
			cb([]);
			return;
		}
		var dirs = d.toString().split('\n');
		var list = [];
		(function process_dirs()
		{
			if(dirs.length == 0)
			{				
				cb(list);
				return;
			}
			var d = dirs.shift();
			d = d.replace(/(\n|\r|\r\n)*/g, '');
			fs.readdir(d, function(err_, files_in)
			{
				if(!err_)
				{
					var files = [];
					files_in.reduce(function(p, c)
					{
						if(rx.test(c))
							p.push({name:c,path:d,full_path:d+'/'+c});
						return p;
					}, files);
					list.push.apply(list, files);
				}
				process_dirs();
			});
		})();
		
	});
}


function is_im(fname, cb){
	list_files(function(d){
		var found = false;
		for(var i = 0; i < d.length; i++){
			if(d[i].full_path == fname){
				found = true;
				break;
			}
		}
		if(found) cb(true);
		else cb(false);
	});
}

exports.list_files = list_files;
exports.is_im = is_im;



