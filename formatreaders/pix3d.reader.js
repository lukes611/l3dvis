var fs = require('fs');


exports.load = function(fname, cb)
{
	fs.readFile(fname, function(err, d)
	{
		if(err)
		{
			cb({type:"pix3d", fname:fname, error:true});
		}else
		{
			var rv = {type:"pix3d", fname:fname, error:false};
			rv.points = [];
			rv.colors = [];
			var len = 0, min = 0, max = 0;
			var USHRT_MAX = 65535;
			len = d.readUInt32LE(0);
			min = d.readDoubleLE(4);
			max = d.readDoubleLE(12);
			d = d.slice(11);
			var X, Y, Z, R, G, B;
			for(var i = 0, j = 0; i < len; i++, j+=9)
			{
				X = d.readUInt16LE(j);
				Y = d.readUInt16LE(j+2);
				Z = d.readUInt16LE(j+4);
				R = d.readUInt8(j+6);
				G = d.readUInt8(j+7);
				B = d.readUInt8(j+8);
				X = (X / USHRT_MAX) * max + min;
				Y = (Y / USHRT_MAX) * max + min;
				Z = (Z / USHRT_MAX) * max + min;
				rv.points.push({x:X,y:Y,z:Z});
				rv.colors.push({r:R,g:G,b:B});
				
			}
			cb(rv);
		}
	});
};


