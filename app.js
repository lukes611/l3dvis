var express = require('express');
var routes = require('./server/routes.js');

var app = express();

routes(app);

app.use(express.static('./client'));

var server = app.listen(3000, function()
{
	var host = server.address().address;
	var port = server.address().port;
	console.log('l3dvi [running[%s,%s]]', host,port);
});



