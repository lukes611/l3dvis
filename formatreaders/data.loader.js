var obj_reader = require('./obj.reader.js');
var si_reader = require('./si.reader.js');
var pix3d_reader = require('./pix3d.reader.js');

exports.load = function(fname, cb)
{
	var objrx = /^(\s|\S)+.obj$/;
	var sirx = /^(\s|\S)+.si$/;
	var pix3drx = /^(\s|\S)+.pix3d$/;
	var imgrx = /^(\s|\S)+.(jpg|png|gif|bmp){1}$/;
	
	var return_f = function(rv){ cb(rv); };
	
	if(objrx.test(fname))
	{//load object
		obj_reader.load(fname, return_f);
	}else if(sirx.test(fname))
	{//load si object
		si_reader.load(fname, return_f);
	}else if(pix3drx.test(fname))
	{//load pix3d
		pix3d_reader.load(fname, return_f);
	}else if(imgrx.test(fname))
	{//load img
		var imtype = fname.split('.').pop();
		cb({type:"img", fname:fname, error:false, imgType : imtype});
	}else
	{
		cb({type:"unknown", fname:fname, error:true});
	}
};

