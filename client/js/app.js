
var gfx = undefined;

//initialize app
$(document).ready(function()
{
	gfx = new L3DV();
	setup_button1();
	load_list();
	var draw = function()
	{
		gfx.draw();
		requestAnimationFrame(draw);
	};
	draw();
	$(window).bind('mousewheel', function(e){
		var mouse_wheel_up = e.originalEvent.wheelDelta/120 > 0;
		if(mouse_wheel_up)
			gfx.move_camera_z(10);
		else gfx.move_camera_z(-10);
	});
	var mouse_x = 0, mouse_y = 0, btn_down = false;
	$(window).mousedown(function(e){
		if(e.button == 0){
			mouse_x = e.clientX;
			mouse_y = e.clientY;
			btn_down = true;
		}
	});
	$(window).mouseup(function(e){
		if(e.button == 0)
			btn_down = false;
	});
	$(window).mousemove(function(e)
	{
		if(btn_down){
			var dx = e.clientX - mouse_x;
			var dy = e.clientY - mouse_y;
			mouse_x = e.clientX;
			mouse_y = e.clientY;
			gfx.rotate(dy/5, dx/5);
		}
	});
});

function load_data(fpn)
{
	$.post('/load', {full_path:fpn},
	function(data, status)
	{
		gfx.load(data);
	});
}

//update list of files
function load_list()
{
	var ds = $('#data_selector');
	ds.empty();
	$.get('/files', function(data, status)
	{
		for(var i = 0; i < data.length; i++)
		{
			var btn = $('<button class="page_font data_elem1" >'+data[i].name+'</button>');
			btn.click(
				{index:i},
				function(event)
				{
					var index = event.data.index;
					var fpn = data[index].full_path;
					load_data(fpn);
				}
			);
			ds.append(btn);
		}
	});
}


//setup the toggle button
function setup_button1()
{
	var visible = true;
	var btn = $('#button1');
	var data_selector = $('#data_selector');
	btn.click(function()
	{
		if(visible)
		{
			data_selector.slideUp();
		}else
		{
			data_selector.slideDown();
		}
		visible = !visible;
	});
}