
var fs = require('fs');

function number_string_list(list)
{
	var rv = [];
	for(var i = 0; i < list.length; i++)
		rv.push(Number(list[i]));
	return rv;
}

exports.load = function(fname, cb)
{
	fs.readFile(fname, function(err, data)
	{
		if(err)
		{
			cb({type:"obj", fname:fname, error:true});
		}else
		{
			var rv = {type:"obj", fname:fname, error:false};
			rv.points = [];
			rv.triangles = [];
			rv.normals = [];
			rv.uvs = [];
			rv.uvis = [];
			rv.normalis = [];
			rv.objects = [];
			var lines = data.toString().split('\n');
			var nl = lines.length;
			var vertReG = /^v ([0-9]*(.[0-9]*)?){3}/;
			var vertNormalReG = /^vn ([0-9]*(.[0-9]*)?){3}/;
			var textureReG = /^vt ([0-9]*(.[0-9]*)?){2}/;
			var poly1 = /f\s([0-9]+\s)+/; //faces only
			var poly2 = /f\s([0-9]+\/[0-9]+\s)+/; //faces and textures
			var poly3 = /f\s([0-9]+\/\/[0-9]+\s)+/; //faces and normals
			var poly4 = /f\s([0-9]+\/[0-9]+\/[0-9]+\s)+/; //faces, textures and normals
			var object_index_regx = /o [a-z]+/i;
			for(var i = 0; i < nl; i++)
			{
				var line = lines[i].replace(/\n/g, '');
				if(vertReG.test(line))
				{
					var elems = line.split(' '); elems.shift(); elems = number_string_list(elems);
					var newVertex = {x : Number(elems[0]), y : Number(elems[1]), z : Number(elems[2])};
					rv.points.push(newVertex);
				}else if(vertNormalReG.test(line))
				{
					var elems = line.split(' '); elems.shift(); elems = number_string_list(elems);
					var newNorm = {x : elems[0], y : elems[1], z : elems[2]};
					rv.normals.push(newNorm);
				}else if(textureReG.test(line))
				{
					var elems = line.split(' '); elems.shift(); elems = number_string_list(elems);
					var newTex = {x : elems[0], y : elems[1]};
					rv.uvs.push(newTex);
				}else if(poly1.test(line))
				{
					var elems = line.split(' '); elems.shift(); elems = number_string_list(elems);
					if(elems.length < 3) continue;
					for(var j = 1; j < elems.length-1; j++)
					{
						rv.triangles.push({x : elems[0]-1, y : elems[j]-1, z : elems[j+1]-1});
					}
				}else if(poly2.test(line))
				{
					var e = line.split(' '); e.shift();
					if(e.length < 3) continue;
					for(var j = 1; j < e.length-1; j++)
					{
						rv.triangles.push({x : Number(e[0].split('/')[0])-1, y : Number(e[j].split('/')[0])-1, z : Number(e[j+1].split('/')[0])-1});
						rv.uvis.push({x : Number(e[0].split('/')[1])-1, y : Number(e[j].split('/')[1])-1, z : Number(e[j+1].split('/')[1])-1});
					}
				}else if(poly3.test(line))
				{
					var e = line.split(' '); e.shift();
					if(e.length < 3) continue;
					var sp = /\/\//g;
					for(var j = 1; j < e.length-1; j++)
					{
						rv.triangles.push({x : Number(e[0].split(sp)[0])-1, y : Number(e[j].split(sp)[0])-1, z : Number(e[j+1].split(sp)[0])-1});
						rv.normalis.push({x : Number(e[0].split(sp)[1])-1, y : Number(e[j].split(sp)[1])-1, z : Number(e[j+1].split(sp)[1])-1});
					}
				}else if(poly4.test(line))
				{
					var e = line.split(' '); e.shift();
					if(e.length < 3) continue;
					var sp = '/';
					for(var j = 1; j < e.length-1; j++)
					{
						rv.triangles.push({x : Number(e[0].split(sp)[0])-1, y : Number(e[j].split(sp)[0])-1, z : Number(e[j+1].split(sp)[0])-1});
						rv.uvis.push({x : Number(e[0].split(sp)[1])-1, y : Number(e[j].split(sp)[1])-1, z : Number(e[j+1].split(sp)[1])-1});
						rv.normalis.push({x : Number(e[0].split(sp)[2])-1, y : Number(e[j].split(sp)[2])-1, z : Number(e[j+1].split(sp)[2])-1});
					}
				}else if(object_index_regx.test(line))
				{
					var ob = {vfrom:rv.points.length,ffrom:rv.triangles.length,vto:-1, fto: -1, name : line.split(' ')[1]};
					if(rv.objects.length > 0)
					{
						var l = rv.objects.length-1;
						rv.objects[l].vto = rv.points.length-1;
						rv.objects[l].fto = rv.triangles.length-1;
					}
					rv.objects.push(ob);
				}
			}
			if(rv.objects.length > 0)
			{
				var l = rv.objects.length-1;
				rv.objects[l].vto = rv.points.length-1;
				rv.objects[l].fto = rv.triangles.length-1;
			}
			cb(rv);
		}
	});
};




