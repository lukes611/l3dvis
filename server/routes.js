var fs = require('fs');
var qs = require('querystring');
var lf = require('./list.files.js');
var dl = require('../formatreaders/data.loader');

module.exports = function(app)
{
	app.get('/', function(req, res, next)
	{
		fs.readFile('./client/html/index.html', function(e,d)
		{
			if(!e) res.send(d.toString());
			else res.send('cannot find html file: index.html');
		});
	});
	
	app.get('/files', function(req, res, next)
	{
		lf.list_files(function(rv)
		{
			res.json(rv);
		});
	});
	
	app.get('/textureload', function(req, res, next){
		
		var full_path = req.query['full_path'];
		lf.is_im(full_path, function(is_im){
			if(is_im){
				var img = fs.readFileSync(full_path);
				var type = full_path.split('.').pop();
				res.writeHead(200, {'Content-Type': 'image/' + type });
				res.end(img, 'binary');
			}
		});
	});
	
	app.post('/load', function(req, res, next)
	{
		var input = '';
		req.on('data', function(d)
		{
			input += d;
			if (input.length > 1e7)
			{
				req.connection.destroy();
			}
		});
		req.on('end', function()
		{
			input = qs.parse(input);
			var full_path = input.full_path;
			dl.load(full_path, function(d)
			{
				res.json(d);
			});
		});
		
	});
};