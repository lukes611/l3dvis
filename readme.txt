A visualization tool for my research.
Opens and views: .obj, .si, .pix3d, and image files::

format readers->
obj:
	{
		type: 'obj'
		points: list of 3d point objects,
		triangles : list of [t1, t2, t2] (ints) triangle indexes,
		normals : [{x,y,z}]
		uvs : [{x,y}]
		uvis : [{x,y,z}]
		normalis : [{x,y,z}]
		objects : {vfrom, vto, ffrom, fto, name} //vertex from, vertex to
	}
.si:
	{
		type: 'si'
		points: same as obj
		triangles : same as obj
	}
.pix3d
	{
		type: 'pix3d'
		points: same as obj,
		colors : [{r:,g:,b:} ... ]
	}
image files:
	{
		type: 'img'
		src name: (on server)
	}